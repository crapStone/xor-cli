use std::{
    env,
    fs::File,
    io::{self, BufReader, Read, Write},
    iter,
};

#[cfg(feature = "dhat-heap")]
#[global_allocator]
static ALLOC: dhat::Alloc = dhat::Alloc;

fn main() {
    #[cfg(feature = "dhat-heap")]
    let _profiler = dhat::Profiler::new_heap();

    let file_data =
        BufReader::new(File::open(env::args().nth(1).expect("two arguments expected")).unwrap());
    let file_xor =
        BufReader::new(File::open(env::args().nth(2).expect("two arguments expected")).unwrap());

    let mut stdout = io::stdout();

    iter::zip(
        file_data.bytes().map(Result::unwrap),
        file_xor.bytes().map(Result::unwrap),
    )
    .map(|(d, k)| d ^ k)
    .for_each(|x| {
        stdout.write_all(&[x]).unwrap();
    });
}
